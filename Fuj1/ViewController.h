//
//  ViewController.h
//  Fuj1
//
//  Created by Mohammed Salah on ٢٤‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController


@property (nonatomic,strong) IBOutlet UITableView * table;

@end
